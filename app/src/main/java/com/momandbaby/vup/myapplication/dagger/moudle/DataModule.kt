package com.momandbaby.vup.myapplication.dagger.moudle

import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.momandbaby.vup.myapplication.app.MomAndBaby
import dagger.Module
import dagger.Provides
import io.realm.Realm
import io.realm.RealmConfiguration
import javax.inject.Singleton

@Module
class DataModule(val app: MomAndBaby) {
    @Provides
    @Singleton
    internal fun provideSharedPreferences(): SharedPreferences =
            PreferenceManager.getDefaultSharedPreferences(app)

    @Provides
    @Singleton
    fun provideDatabase(): Realm {
        Realm.init(app)
        val config = RealmConfiguration.Builder().deleteRealmIfMigrationNeeded().name("buzi").build()
        Realm.setDefaultConfiguration(config)
        return Realm.getDefaultInstance()
    }
}

