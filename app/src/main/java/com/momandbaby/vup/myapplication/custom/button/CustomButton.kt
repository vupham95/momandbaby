package com.momandbaby.vup.myapplication.custom.button

import android.content.Context
import android.util.AttributeSet
import android.widget.Button
import com.momandbaby.vup.myapplication.manage.FontManager


class CustomButton : Button {

    constructor(context: Context?) : super(context) {
        init(context, null)
    }

    constructor(context: Context?, attributeSet: AttributeSet) : super(context, attributeSet) {
        init(context, attributeSet)
    }

    constructor(context: Context?, attributeSet: AttributeSet, defStyleAttr: Int): super(context, attributeSet, defStyleAttr) {
        init(context, attributeSet)
    }

    private fun init(context: Context?, attributeSet: AttributeSet?) {
        //this.setTextSize(1,24f)
        typeface = FontManager.getBariolRegularFont(context!!)
    }

}