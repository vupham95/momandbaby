package com.momandbaby.vup.myapplication.view.splash_activity

import android.annotation.SuppressLint
import android.media.MediaPlayer
import android.os.Build
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.*

import com.momandbaby.vup.myapplication.R
import com.momandbaby.vup.myapplication.utils.Navigator
import com.momandbaby.vup.myapplication.view.main_activity.MainActivity

@SuppressLint("Registered")
class SplashActivity : AppCompatActivity() {

    private var mMediaPlayer: MediaPlayer? = null
    private var mFirstSurface: SurfaceHolder? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.controller_splash_screen)
        toggleHidKeyBar()
        initIntroVideo()

    }

    private fun initIntroVideo() {
        val first = findViewById<View>(R.id.surfaceView1) as SurfaceView
        first.holder.addCallback(object : SurfaceHolder.Callback {
            override fun surfaceCreated(surfaceHolder: SurfaceHolder) {
                mFirstSurface = surfaceHolder
                mMediaPlayer = MediaPlayer.create(applicationContext, R.raw.video)
                mMediaPlayer!!.setDisplay(mFirstSurface)
                mMediaPlayer!!.start()
                mMediaPlayer!!.setOnCompletionListener { mediaPlayer ->
                    mediaPlayer.release()
                    goToMain()
                }
            }

            override fun surfaceChanged(surfaceHolder: SurfaceHolder, i: Int, i2: Int, i3: Int) {

            }

            override fun surfaceDestroyed(surfaceHolder: SurfaceHolder) {}
        })
    }

    private fun goToMain() {
        Navigator.startActivity(this@SplashActivity, MainActivity::class.java)
    }

    @SuppressLint("ObsoleteSdkInt")
    fun toggleHidKeyBar() {
        var newUiOptions = window.decorView.systemUiVisibility
        if (Build.VERSION.SDK_INT >= 14) {
            newUiOptions = View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
        }
        if (Build.VERSION.SDK_INT >= 16) {
            newUiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN
        }
        if (Build.VERSION.SDK_INT >= 19) {
            newUiOptions = View.SYSTEM_UI_FLAG_IMMERSIVE
        }
        window.decorView.systemUiVisibility = newUiOptions
    }
}
