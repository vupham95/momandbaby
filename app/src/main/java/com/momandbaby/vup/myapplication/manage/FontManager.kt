package com.momandbaby.vup.myapplication.manage

import android.content.Context
import android.graphics.Typeface

class FontManager {
    companion object {
        var bariolRegularFont: Typeface? = null

        fun getBariolRegularFont(context: Context): Typeface? {
            if (bariolRegularFont == null) {
                bariolRegularFont = Typeface.createFromAsset(context?.assets, "Bariol_Regular.otf")!!
            }

            return bariolRegularFont
        }
    }
}