package com.momandbaby.vup.myapplication.view.main_activity

import android.os.Bundle
import android.view.ViewGroup
import butterknife.BindView
import com.bluelinelabs.conductor.Conductor
import com.bluelinelabs.conductor.Router
import com.bluelinelabs.conductor.RouterTransaction
import com.momandbaby.vup.myapplication.R

import com.momandbaby.vup.myapplication.app.MomAndBaby
import com.momandbaby.vup.myapplication.view.base.BaseActivity

class MainActivity : BaseActivity<MainPresenter>() {

    @BindView(R.id.controller_container)
    lateinit var container: ViewGroup

    private lateinit var router: Router

    override fun onCreate(savedInstanceState: Bundle?) {
        (applicationContext as MomAndBaby).appComponent.inject(this)
        super.onCreate(savedInstanceState)

        router = Conductor.attachRouter(this, container, savedInstanceState)
//        if (!router.hasRootController()) {
//            router.setRoot(RouterTransaction.with(SplashController()))
//        }

    }

    override fun newPresenter(): MainPresenter {
        return MainPresenter()
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_main
    }

    override fun onBackPressed() {
        if (!router.handleBack())
            super.onBackPressed()

    }

}
