package com.momandbaby.vup.myapplication.view.base

import android.content.Context

interface BaseView<in P> {
    fun setPresenter(presenter: P)
    fun getContext(): Context
    fun isFinishing(): Boolean
    fun disconnect()
}