package com.momandbaby.vup.myapplication.dagger.component

import com.momandbaby.vup.myapplication.app.MomAndBaby
import com.momandbaby.vup.myapplication.dagger.moudle.AppModule
import com.momandbaby.vup.myapplication.dagger.moudle.DataModule
import com.momandbaby.vup.myapplication.view.main_activity.MainActivity
import com.momandbaby.vup.myapplication.view.splash_activity.SplashActivity
import dagger.Component
import javax.inject.Singleton


@Singleton
@Component(modules = arrayOf(AppModule::class, DataModule::class))
interface AppComponent {
    fun inject(app: MomAndBaby)
    fun inject(mainActivity: MainActivity)
    fun inject(splashActivity: SplashActivity)
}