package com.momandbaby.vup.myapplication.view.base

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import butterknife.ButterKnife
import butterknife.Unbinder
import com.bluelinelabs.conductor.Controller

abstract class BaseController<P : BasePresenter> : Controller() {
    protected var presenter: P? = null

    abstract fun getControllerLayoutId(): Int
    abstract fun inject()
    abstract fun onControllerCreated()
    protected abstract fun newPresenter(): P

    private var unbinder: Unbinder? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup): View {
        val view = inflater.inflate(getControllerLayoutId(), container, false)
        unbinder = ButterKnife.bind(this, view)
        inject()

        if (presenter == null) {
            presenter = newPresenter()
        }
        onControllerCreated()
        return view
    }

    override fun onActivityPaused(activity: Activity) {
        super.onActivityPaused(activity)
        presenter?.disConnect()
    }

    override fun onDestroyView(view: View) {
        super.onDestroyView(view)
        unbinder!!.unbind()
        unbinder = null
        presenter?.disConnect()
    }
}