package com.momandbaby.vup.myapplication.custom.textview

import android.content.Context
import android.support.v7.widget.AppCompatTextView
import android.util.AttributeSet
import com.momandbaby.vup.myapplication.manage.FontManager


class MABTextView : AppCompatTextView {

    constructor(context: Context?) : super(context) {
        init(context, null)
    }

    constructor(context: Context?, attributeSet: AttributeSet) : super(context, attributeSet) {
        init(context, attributeSet)
    }

    constructor(context: Context?, attributeSet: AttributeSet, defStyleAttr: Int): super(context, attributeSet, defStyleAttr) {
        init(context, attributeSet)
    }

    private fun init(context: Context?, attributeSet: AttributeSet?) {
        //this.setTextSize(1,24f)
        typeface = FontManager.getBariolRegularFont(context!!)
    }

}