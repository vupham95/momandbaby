package com.momandbaby.vup.myapplication.dagger.moudle

import com.momandbaby.vup.myapplication.app.MomAndBaby
import dagger.Module

@Module
class AppModule(private val app: MomAndBaby)
