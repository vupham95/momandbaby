package com.momandbaby.vup.myapplication.view.base

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import butterknife.ButterKnife

abstract class BaseActivity<P : BasePresenter> : AppCompatActivity() {

    private lateinit var presenter: P

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(getLayoutId())

        ButterKnife.bind(this)

        presenter = newPresenter()
    }

    override fun onPause() {
        super.onPause()

        presenter.disConnect()
    }

    abstract fun getLayoutId(): Int

    abstract fun newPresenter(): P
}