package com.momandbaby.vup.myapplication.utils;

import android.app.Activity;
import android.content.Intent;

import com.momandbaby.vup.myapplication.R;

public class Navigator {
    public static void startActivity(Activity currentActivity, Class targetActivityClass) {
        Intent intent = new Intent(currentActivity, targetActivityClass);
        currentActivity.startActivity(intent);
        currentActivity.overridePendingTransition(R.anim.trans_left_in, R.anim.trans_left_out);
    }
    public static void onBackActivity(Activity activity) {
        activity.onBackPressed();
    }
    public static void overridePendingTransition(Activity currentActivity) {
        currentActivity.overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
    }
}
