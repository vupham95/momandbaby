package com.momandbaby.vup.myapplication.app;

import android.app.Application;

import com.momandbaby.vup.myapplication.dagger.component.AppComponent;
import com.momandbaby.vup.myapplication.dagger.component.DaggerAppComponent;
import com.momandbaby.vup.myapplication.dagger.moudle.AppModule;
import com.momandbaby.vup.myapplication.dagger.moudle.DataModule;
import com.momandbaby.vup.myapplication.utils.Const;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class MomAndBaby extends Application {
    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(this);
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder().build();
        Realm.setDefaultConfiguration(realmConfiguration);
        Const.appContext = getApplicationContext();
        initDaggerGraph();
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }

    private void initDaggerGraph() {
        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .dataModule(new DataModule(this))
                .build();
    }
}
